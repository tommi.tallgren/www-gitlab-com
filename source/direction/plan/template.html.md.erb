---
layout: markdown_page
title: "Product Vision - Plan"
---

- TOC
{:toc}

The product vision of the Plan stage of the [DevOps lifecycle](/direction/#devops-stages) in GitLab is to enable
all people in an organization to innovate ideas, turn those ideas into transparent plans,
track their execution, adjust the plans along the way as needed, and evaluate the efficacy of the entire process, 
providing actionable insight for continuous improvement. This is enabled through team members,
internal and external stakeholders, and executive sponsors all collaborating on the same single-source-of-truth
artifacts throughout the application.

## Detailed Plan product vision

Each area of the Plan product vision is shown below. It is ever-changing as we incorporate more ideas from users, customers,
and community members. Also see a 
[timeline-based roadmap view of these planned upcoming improvements](https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=Plan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS).

### Portfolio management with epics and roadmaps

GitLab portfolio management (together with project management below) allows different team members
and stakeholders throughout your organization to innovate and collaborate on high-level ideas;
turn those into strategic initiatives with executive sponsorship; scope and estimate work effort required;
plan that work across quarters and years; and execute it in weeks and months.

- [Sorting in epics list view and roadmap view](https://gitlab.com/groups/gitlab-org/-/epics/236)
- [Epic email notifications, todos, and autocomplete](https://gitlab.com/groups/gitlab-org/-/epics/148)
- [Close epics](https://gitlab.com/groups/gitlab-org/-/epics/145)
- [Epics and roadmaps integrated with milestones](https://gitlab.com/groups/gitlab-org/-/epics/227)
- [Make epics easier to use](https://gitlab.com/groups/gitlab-org/-/epics/134)
- [Moving forward and backward in time in roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/311)
- [Flexible work breakdown structure with epic relationships](https://gitlab.com/groups/gitlab-org/-/epics/312)
- [Inherit children epics start and due dates](https://gitlab.com/groups/gitlab-org/-/epics/318)
- [Weight and progress information in epics and roadmaps](https://gitlab.com/groups/gitlab-org/-/epics/76)
- [Autoclose epic](https://gitlab.com/groups/gitlab-org/-/epics/327)
- [Milestones in roadmap](https://gitlab.com/groups/gitlab-org/-/epics/329)
- [Searching in epics](https://gitlab.com/groups/gitlab-org/-/epics/315)
- [Polish epic relationships](https://gitlab.com/groups/gitlab-org/-/epics/317)

### Project management with issues and boards

- [Managing many boards](https://gitlab.com/groups/gitlab-org/-/epics/336)
- [Filtering options in system activity](https://gitlab.com/groups/gitlab-org/-/epics/347)
- [Track issue and merge request description changes](https://gitlab.com/groups/gitlab-org/-/epics/237)
- [Resolvable discussions in issues and epics](https://gitlab.com/groups/gitlab-org/-/epics/226)
- [`not` and `or` in search bars](https://gitlab.com/groups/gitlab-org/-/epics/291)
- [Multiple milestones per issue or merge request](https://gitlab.com/groups/gitlab-org/-/epics/69)
- [Group milestones parity with project milestones](https://gitlab.com/groups/gitlab-org/-/epics/6)
- [Filter by project on group lists and group board](https://gitlab.com/groups/gitlab-org/-/epics/330)
- [Move default description templates out of project settings](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Bulk manage issue and merge request subscriptions](https://gitlab.com/groups/gitlab-org/-/epics/156)
- [Move default description templates out of project settings](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Milestone change events for system notes](https://gitlab.com/groups/gitlab-org/-/epics/238)
- [Advanced integrated boards for teams](https://gitlab.com/groups/gitlab-org/-/epics/274)
- [Bulk edit at group level](https://gitlab.com/groups/gitlab-org/-/epics/310)

### Personal workflow management with notifications and todos

GitLab helps you get your work done, personalized to your own specific workflows,
using notifications and todos.

- [Unified todos and notifications](https://gitlab.com/groups/gitlab-org/-/epics/149)

### Governance with enforced workflows and custom fields

Larger enterprises require enhanced governance in their business management and
software delivery, which can be addressed with enforced workflows and custom fields.

- [Enforced workflows](https://gitlab.com/groups/gitlab-org/-/epics/364)
- [Custom fields](https://gitlab.com/groups/gitlab-org/-/epics/235)

### Analytics

GitLab has in-product analytics, helping teams track performance over time,
highligting major problems, and providing the actionable insight to solve them.

- [Burndown charts in boards](https://gitlab.com/groups/gitlab-org/-/epics/230)
- [Analytics chart dashboards](https://gitlab.com/groups/gitlab-org/-/epics/313)
- [Value stream analytics](https://gitlab.com/groups/gitlab-org/-/epics/228)
- [VSM analytics standalone and also integrated into boards](https://gitlab.com/groups/gitlab-org/-/epics/229)
- [Advanced analytics in boards for teams](https://gitlab.com/groups/gitlab-org/-/epics/233)

### Search

Teams leverage GitLab search to quickly search for relevant content, enabling stronger
intra-team and cross-team collaboration through discovery of all GitLab data.

- [Get Elasticsearch working on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/153)
- [Support Elasticsearch beyond Global search](https://gitlab.com/groups/gitlab-org/-/epics/154)

### Jira integration

GitLab supports deep Jira integration, allowing teams to use Jira for issue mangement, but still
leveraging the benefits of GitLab source control and other native GitLab features.

- [Import Jira issues to GitLab issues](https://gitlab.com/groups/gitlab-org/-/epics/10)
- [Better than Atlassian Jira integration](https://gitlab.com/gitlab-org/gitlab-ce/issues/27073)

## Representative flows

### Engineers and designers

Details to come

### Product managers 

Details to come

### Directors

Details to come

### Executives

The CEO works with the VPs of Product and Engineering to develop the high-level product strategy for the next 8 calendar quarters. They create epics and use appropriate labels to indicate that they represent strategic high-level initiatives. They put start and end dates on the epics, representing their first pass approximate timelines and order of delivery. The epics are shared across the company in the roadmap view.

The Product and Engineering teams subsequently innovate on those initiatives, scoping out features and effort estimates which in turn adjust and further refine the epics themselves.

At the same time, the roadmap of epics are shared with the VPs of Sales, Marketing, and Operations, and their respective teams. These teams create, track and socialize their own initiatives using epics as well, that appear in the same roadmap.

Later on in the year, a strategic acquistion opportunity presents itself to the executive team and the board of directors. At the board meeting, they review the roadmap and decide the acquisition fits into the overall company vision and medium term strategy. They proceed to buy the company. With the acquisition, a key product feature originally planned is no longer required. Instead, the Engineering team is now responsible for integrating the technology from the acquired company. The VPs of Product and Engineering make adjustments to the roadmap accordingly. At that same time, the VP of Marketing inserts new epics into the roadmap to produce content and campaigns to explain the impact to customers (including customers of the acquired company). The VPs of Sales and Operations inserts epics to train and integrate the new staff from the acquired company. The entire roadmap is radically changed due to the acquisition, but the process is controlled and carefully managed with executive oversight and collaboration.

## Other areas of interest

- Quality management
- Requirements management

## How we prioritize

Details to come

## Contributions and feedback

We love community code contributions to GitLab. Read [this guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/development/contributing/index.md) to get started.

Please also participate in our [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce). 
You can file bugs, propose new features, suggest design improvements, or continue a conversation 
on any one of these. Simply [open a new issue](https://gitlab.com/gitlab-org/gitlab-ce/issues/new) 
or comment on [an existing one](https://gitlab.com/gitlab-org/gitlab-ce/issues).

You can also contact me (Victor) via the channels on [my profile page](https://gitlab.com/victorwu).