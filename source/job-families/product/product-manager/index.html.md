---
layout: job_page
title: "Product Manager"
---

## Role

We're looking for product managers that can help us work on the future of developer tools. Specifically we're looking for independent people that are interested in building solutions for (very) large teams and organizations.

We know there are a million things we can and want to improve about GitLab. It'll be your job to work out what we are going to do and how.

We work in quite a unique way at GitLab, where lots of flexibility and independence is mixed with a high paced, pragmatic way of working. And everything we do is in the open.

We recommend looking at our [primer](/primer) to get started.

## Responsibilities

- Build an attractive roadmap together with the Head of Product, VP of Product, and CEO; based on our [vision](/direction/#vision)
- Work out feature proposals from the community and customers
- Manage new features tailored for larger enterprises from conception to market
- Work together with UX and Frontend engineers on improving the look and feel of GitLab
- Ensure a smooth release of changes and features together with all stakeholders
- Empower the community by writing great documentation and highlighting the product in various ways in cooperation with marketing
- Finding the weak spots of GitLab and executing plans to improve them
- Work together with other stakeholders on scheduling and executing releases
- Estimating revenue impact of changes
- Have a feel for engineering effort/ROI
- Act as a general manager of a product, taking end-to-end responsibility
- Follow innovation in your product area
- Make change to increase usage of features
- Create demos to showcase current or future features
- Build a vision for the future of a product
- Talk with customers on a regular basis
- Support sales as a subject matter expert

## You are _not_ responsible for

- Shipping in time. As a PM you are part of a team that delivers a change,
the team is responsible for shipping in time, not you.
- A team of engineers. PMs at GitLab do not manage people, they manage the
_product_. You'll be required to take the lead in decisions about the product,
but it's not your role to manage the people that build the product.
- Capacity and availability planning. You will work together with engineering
managers on schedules and planning: you prioritize, the engineering managers
determine how much can be scheduled.

## Requirements

- Experience in product management
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space
- Strong technically. You understand how software is built, packaged and deployed.
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want
- You share our [values](/handbook/values), and work in accordance with those values
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects

## Senior Product Manager

A senior product manager is an experienced product manager that:

1. Consistently displays the ability to ship changes in small iterations
1. Regularly proposes and leads new features
1. Is able to distill proposals and requests from the community and customers to changes in GitLab
1. Makes significant contributions to product marketing and makes sure this is up to date
1. Shows deep understanding of their area and has a vision of where to go next
1. Work independently and pro-actively
1. Excites others with their work

## Specialties

### Meltano (BizOps Product)

A product manager to invent the future of BizOps tools; specifically, taking our approach to Complete DevOps, and applying it to [Meltano](https://gitlab.com/meltano/meltano), an open source convention-over-configuration product for data engineering, analytics, business intelligence, and data science; leveraging version control, CI, CD, Kubernetes, and review apps. We aim to help customers answer such questions as how can I acquire the highest customer lifetime value (LTV) at the lowest customer acquisition cost (CAC). Think about creating open source and deeply-integrated tools without using traditional BI tools.

This is a new area for GitLab and one of our big-bets for 2018/2019. It'll be your job to work out what we are going to do and how. In some ways, we need a full-stack PM; someone that can write a little code, update a pipeline, or whatever is necessary in a very small team acting like a startup-in-a-startup.

#### Requirements

- Strong understanding of business operations, business intelligence, data warehousing, etc.
- Experience with CI/CD and containers. Knowledge of Kubernetes and GitLab CI/CD a plus.

### CI/CD

Product managers that can help us work on the future of DevOps tools; specifically, building out continuous integration (CI), continuous delivery (CD), release automation (RA), and beyond.

#### Responsibilities

- Find the weak spots of GitLab CI/CD and execute plans to improve them

#### Requirements

- Strong understanding of CI/CD and Release Automation
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker

### Configuration

Product managers to help us work on the future of DevOps tools; specifically, building out configuration management and other operations-related features such as feature flags, and ChatOps.

#### Requirements

- Strong understanding of CI/CD, configuration management, and operations
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker

### Distribution and Packaging

Product managers that can help us work on the future of DevOps tools; specifically, building out packaging features such as Docker container registry and binary artifact management, and distribution features as as cloud-native Helm charts.

#### Requirements

- Strong understanding of CI/CD and package management
- Understanding of deployment infrastructure and container technologies
- Significant experience with Kubernetes and Docker
- Significant experience with Java development practices

### Secure

We're looking for product managers that can help us work on the future of developer tools. Specifically, building out application security testing, including static analysis and dynamic testing.

#### Requirements

- Strong understanding of CI/CD and automated security testing
- Understanding of deployment infrastructure and container technologies such as Kubernetes and Docker

### Growth

Get more people to use and contribute to GitLab, mainly the Community Edition (CE). Marketing gets us awareness and downloads. Your goal is to ensure people downloading our product keep using it, invite others, and contribute back. You report to the VP of Product.

#### Goals

1. Conversion of download to 30 day active (install success * retention)
1. Growth of existing CE installations
(growth of comments measured with usage ping)
1. Growth in the number of new people that contributed merged code

#### Possible Improvements

1. Auto install let's encrypt
1. Check open to new users by default
1. Check invite by email function
1. Usage ping for CE
1. Ensure installers are very simple
1. First 60 seconds with a product
1. Measure contributors like a SaaS funnel (activation, retention)
1. Improve data for Usage ping EE
1. Improve zero-states of project (hints on first action)
1. New feature show-off / highlighting
1. Great experience when not logged in (commenting)
1. Great experience when not a project member (edit button works)
1. Make it easier to configure email
1. Make it easier to configure LDAP

#### You'll work with

* Other PM's
* Developers (implement some things self, some with help of experts)
* Community team
* Developers
* Merge request coaches

#### Requirements

- Deep understanding of UX and UI
- You've built web applications before in Ruby on Rails (be it personal or professional)
- You're pragmatic and willing to code yourself
- You're able to independently find, report and solve issues and opportunities related to growth in the product
- Good understanding of Git
- Able to make wireframes and write clear, concrete product specifications

#### Responsibilities

- Plan and execute on improvements in GitLab related to growth
- Write specs and create wireframes to communicate your plans
- Ship improvements every month and make it possible to report on those
improvements
- Do data analysis whenever useful
- Assist the rest of the team with topics related to growth
- Build and expand tools related to growth (version.gitlab.com and others)

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)

## Hiring process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with a Head of Product
* Next, second interview with the manager of the specialty
* Candidates will then be invited to schedule a third interview with the VP of Product
* Candidates may be asked to meet with additional team members at the manager's discretion
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
